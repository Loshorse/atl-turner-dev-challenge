const dotenv = require('dotenv').config();

module.exports = {
    mongoUrl: process.env.MONGODB_URL
};