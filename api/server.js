const morgan = require('morgan');

const cors = require('cors');

const port = 9000;

const app = require('express')();

const server = require('http').Server(app);

const logger = require('./utils/Logger');

const routes = require('./routes/index');

app.use(morgan('short', { stream: logger.stream }));

app.use(cors());

app.use(routes);

server.listen(port, (err) => {
  if (err) {
    logger.error('Error', err);
  }

  logger.info(`API is up & running on Andre ${port}`);
});
