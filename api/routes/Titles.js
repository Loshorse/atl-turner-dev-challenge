const router = require('express').Router();

const Title = require('../controllers/Titles');

router.get('/titles', Title.titleSearch);

router.get('/titles/:titleId', Title.getTitleById);

module.exports = router;