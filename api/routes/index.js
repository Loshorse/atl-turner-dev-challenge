const Title = require('./Titles');

const routes = require('express').Router();

routes.use(Title);

module.exports = routes;
