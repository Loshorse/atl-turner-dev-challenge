const { mongoUrl } = require('../config');

const mongo = require('mongodb').MongoClient;

const logger = require('../utils/Logger');

module.exports = {
  /**
   * @description Searches Turner Database for Movie Title
   * @returns The Title document that was found from the query
   */

  titleSearch: async (req, res) => {
    try {
      let client = await mongo.connect(mongoUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });

      let db = client.db('dev-challenge');

      const collection = await db.collection('Titles');

      let title = await collection.findOne({
        TitleName: {
          $regex: `^${req.query.TitleName}`,
          $options: 'i'
        }
      });

      res.json(title);
    } catch (err) {
      logger.error('There was an error', err);

      res.json(err);
    }
  },

  /**
   * @description Get Movie Title from Turner DB via ObjectId
   * @returns The Title document that was found from the query
   */

  getTitleById: async (req, res) => {
    try {
      let client = await mongo.connect(mongoUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });

      let db = client.db('dev-challenge');

      const collection = await db.collection('Titles');

      let title = await collection.findOne({
        _id: req.params.titleId
      });

      res.json(title);
    } catch (err) {
      logger.error('There was an error', err);

      res.json(err);
    }
  }
};
