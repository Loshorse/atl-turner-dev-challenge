import React from 'react';

import axios from 'axios';

import '../App.css';

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = { title: {}, awards: [], participants: [] };
  }

  componentDidMount() {
    const {
      match: { params }
    } = this.props;

    axios
      .get(`http://localhost:9000/titles/${params.id}`)
      .then(response => {
        this.setState(
          {
            title: response.data,
            awards: response.data.Awards,
            participants: response.data.Participants
          },
          () => {
            console.log(this.state);
          }
        );
      })
      .catch(err => {
        console.log('Error logging in', err);
      });
  }

  render() {
    const { title, awards } = this.state;
    return (
      <div className="App pt-3">
        <h1>{title.TitleName}</h1>

        <p>Released in {title.ReleaseYear}</p>

        <p>Awards Won: {awards.length} </p>
      </div>
    );
  }
}
