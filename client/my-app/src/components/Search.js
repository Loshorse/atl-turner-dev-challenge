import React from 'react';

import { Link } from 'react-router-dom';

import { Jumbotron, Button } from 'react-bootstrap';

import '../App.css';

import axios from 'axios';

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '', title: {} };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    axios
      .get(`http://localhost:9000/titles?TitleName=${this.state.value}`)
      .then(response => {
        console.log(response.data);

        this.setState({ title: response.data });
      })
      .catch(err => {
        console.log('Error logging in', err);
      });

    event.preventDefault();
  }

  render() {
    const ShowTitleInfo = ({ title }) => {
      if (title === null) {
        return (
          <div>
            <p>
              No results found. Please be sure to check your spelling or search
              for another title.
            </p>
          </div>
        );
      } else {
        return (
          <div>
            <h1>
              {' '}
              <Link to={`/${title._id}`}> {title.TitleName} </Link>
            </h1>
          </div>
        );
      }
    };

    const { title } = this.state;

    return (
      <div className="App">
        <Jumbotron>
          <h1> Award Winners</h1>

          <p className="lead">
            Search and get all info about your favorite titles!
          </p>

          <form onSubmit={this.handleSubmit}>
            <input
              type="text"
              placeholder="Enter the title..."
              value={this.state.value}
              onChange={this.handleChange}
            />

            <input className="btn-primary" type="submit" value="Submit" />
          </form>
        </Jumbotron>

        <ShowTitleInfo title={title} />
      </div>
    );
  }
}
