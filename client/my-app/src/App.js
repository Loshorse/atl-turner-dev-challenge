import React from 'react';

import '.././src/App.css'

import Search from './components/Search';

import TitleDetails from './components/TitleDetails';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Search} exact />
        <Route path="/:id" component={TitleDetails} />
      </Switch>
    </Router>
  );
}

export default App;
